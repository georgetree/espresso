package main

import (
	_ "Espresso/docs"
	"Espresso/models"
	"Espresso/server"
	"fmt"
	"os"

	_ "github.com/joho/godotenv/autoload"
)

// @version 0.0.1
// @contact.name davidleitw
// @contact.email davidleitw@gmail.com
// @description Expreesso calendar的api文檔, 方便串接前後端
// @license.name Apache 2.0
// @host http://espresso.nctu.me:8080/
// @BasePath /api/

var dbusername = os.Getenv("DATABASE_USER")
var password = os.Getenv("DATABASE_PASSWORD")
var dbName = os.Getenv("DATABASE_NAME")
var dbHost = os.Getenv("DATABASE_HOST")

var postgreslogin = fmt.Sprintf("host=%s user=%s dbname=%s sslmode=disable password=%s", dbHost, dbusername, dbName, password)

func main() {
	models.ConnectDataBase(postgreslogin)

	defer models.DB.Close()
	r := server.NewRouter()
	r.Run()
}
